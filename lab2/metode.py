from DFA import *
import sys

def stvori():
    automat = DFA()
    automat.Q = sorted(tuple(obradi(input(), ",")))
    automat.sigma = sorted(tuple(obradi(input(), ",")))
    automat.accept = sorted(tuple(obradi(input(), ",")))
    automat.pocetno = input()

    lista_prijelaza = []
    for line in sys.stdin:
        lista_prijelaza.append(line)

    automat.delta = stvori_prijelaze(lista_prijelaza)

    eprint("Stanja: " + str(automat.Q))
    eprint("Abeceda: " + str(automat.sigma))
    eprint("Prihvatljiva: " + str(automat.accept))
    eprint("Pocetno: " + str(automat.pocetno))
    eprint("Prijelazi: " + str(automat.delta))

    return automat

def obradi(string, delimiter):
    lista_stringova = string.split(delimiter)
    skup = set(lista_stringova)
    return skup

def stvori_prijelaze(delta):
    dicc = {}

    for prijelaz in delta:
        parts = prijelaz.split("->")
        if "\n" in parts[1]:
            parts[1] = parts[1][:-1]
            parts[1] = parts[1].split(",")

        if parts[0] not in dicc:
            dicc[parts[0]] = parts[1]
        else:
            parts[1] += dicc[parts[0]]
            dicc[parts[0]] = parts[1]

    return dicc
