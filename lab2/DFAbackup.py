from __future__ import print_function
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class DFA:
    Q = []
    sigma = []
    delta = {}
    pocetno = None
    accept = []

    def __init__(self):
        Q = []
        sigma = []
        delta = {}
        pocetno = None
        accept = []

#==================================================
#             ELIMINACIJA NEDOHVATNIH
#==================================================

    def eliminiraj_nedohvatna(self):
        dohvatljiva_stanja = []
        dohvatljiva_stanja.append(self.pocetno)
        doslo_do_promjene = True

        while doslo_do_promjene:
            doslo_do_promjene = False

            kandidati = []
            for stanje in dohvatljiva_stanja:
                for znak in self.sigma:
                    kljuc = stanje + "," + znak
                    novi = (self.delta[kljuc])
                    novi = str(novi).split("'")
                    kandidati.append(novi[1])

            for kandidat in kandidati:
                if kandidat not in dohvatljiva_stanja:
                    dohvatljiva_stanja.append(kandidat)
                    doslo_do_promjene = True

        self.Q = sorted(tuple(dohvatljiva_stanja))

        temp = []
        for stanje in self.accept:
            if stanje in self.Q:
                temp.append(stanje)
        self.accept = temp

        self.eliminiraj_prijelaze()

        eprint("Nova dohvatljiva su: " + str(self.Q))
        eprint("Nova prihvatljiva su: " + str(self.accept))
        eprint("Nova funkcija prijelaza je: " + str(self.delta))

        return

    def eliminiraj_prijelaze(self):
        kljucevi = self.delta.keys()
        za_izbaciti = []

        for key in kljucevi:
            lista = key.split(",")
            if lista[0] not in self.Q:
                za_izbaciti.append(key)

        for kljuc in za_izbaciti:
            self.delta.pop(kljuc, None)

        return

#==================================================
#             ELIMINACIJA ISTOVJETNIH
#==================================================

    def init_provjere(self, lista):
        i, j = 0, 0

        for i in range(len(self.Q)-1):
            while (j < len(self.Q)):
                for k in range(len(lista)):
                    print(lista[k])
                lista.append(self.Q[i])
                lista.append(self.Q[j])

                for l in range(len(lista)):
                    print(lista[l])
        return

    def provjeri_istovjetnost(self, p, q, lista):
        lista_provjerenih = lista

        if (((p in self.accept and q not in self.accept))
                or ((p not in self.accept) and (q in self.accept))):
            return False

        for znak in self.sigma:
            novi_p = self.delta[str(p) + "," + znak]
            novi_q = self.delta[str(q) + "," + znak]

            if ((novi_p is None and novi_q is not None)
                    or (novi_p is not None) and (novi_q is None)):
                return False

        if (novi_p is not None and novi_q is not None):
            lista_provjerenih.append(novi_p)
            lista_provjerenih.append(novi_q)

        if (p is q and novi_p is novi_q):
            return False
        elif p in lista_provjerenih:
            lista_provjerenih.remove(p)
        if q in lista_provjerenih:
            lista_provjerenih.remove(q)

        if not lista_provjerenih:
            return True

        if self.provjeri_istovjetnost(lista_provjerenih[0], lista_provjerenih[1], lista) is True:
            return True
        else:
            return False

#==================================================
#                  UTILITY METODE
#==================================================
