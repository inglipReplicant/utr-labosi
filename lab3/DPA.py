from __future__ import print_function
from copy import deepcopy
import sys

def eprint(*args, **kwargs):
    '''Ispisuje na stderr umjesto stdin.'''

    print(*args, file=sys.stderr, **kwargs)

class DPA:
    '''Simulator deterministickog potisnog automata.'''

    __nizovi = tuple()
    __stanja = tuple()
    __sigma = tuple()
    __znakovi_stoga = tuple()
    __prihvatljiva = tuple()
    __pocetno = None
    __pocetak_stoga = None
    __delta = {}
    __trenutno_stanje = None
    __stog = []
    __ispis = None

    def __init__(self):
        self.parse_input()

    def parse_input(self):
        '''Parsira input, tj. stvara DPA.'''

        self.__nizovi = tuple(input().split("|"))
        self.__stanja = tuple(input().split(","))
        self.__sigma = tuple(input().split(","))
        self.__znakovi_stoga = tuple(input().split(","))
        self.__prihvatljiva = tuple(input().split(","))
        self.__pocetno = input()
        self.__pocetak_stoga = input()
        self.__delta = {}
        self.sredi_deltu()
        self.__trenutno_stanje = self.__pocetno
        self.__stog = list(self.__pocetak_stoga)

        self.kontrolni_ispis()
        return

    def sredi_deltu(self):
        '''Stvara mapu prijelaza.'''

        for line in sys.stdin:
            elementi = line.split("->")
            kljuc = tuple(elementi[0].split(","))
            ele_novo = elementi[1].split(",")
            ele_novo[1] = ele_novo[1][:-1]
            novo_stanje = tuple(ele_novo)
            self.__delta[kljuc] = novo_stanje

        return

    def kontrolni_ispis(self):
        '''Ispisuje sve postavljene vrijednosti nakon parsiranja.'''

        eprint("1) Ulazni nizovi su: " + str(self.__nizovi))
        eprint("2) Q: " + str(self.__stanja))
        eprint("3) Sigma: " + str(self.__sigma))
        eprint("4) Znakovi stoga: " + str(self.__znakovi_stoga))
        eprint("5) F: " + str(self.__prihvatljiva))
        eprint("6) Pocetno: " + str(self.__pocetno))
        eprint("7) Pocetni znak stoga: " + str(self.__pocetak_stoga))
        eprint("8) Delta: " + str(self.__delta))
        eprint("9) Treutno stanje: " + str(self.__trenutno_stanje))
        eprint("10) Stog: " + str(self.__stog))

        return

#=====================================================================
#                            SIMULATOR
#=====================================================================

    def simuliraj(self):
        for niz in self.__nizovi:
            podijeljeni_niz = niz.split(",")
            self.__ispis = ""

            for znak in podijeljeni_niz:
                vrh_stoga = self.izvuci_znak()
                kljuc = (self.__trenutno_stanje, znak, vrh_stoga)
                eprint("Kljuc je: " + str(kljuc))

                try:
                    novi = self.__delta[kljuc]
                    eprint(str(novi))
                except:
                    eprint("Greska.")

    def izvuci_znak(self):
        eprint("Stog je: " + str(self.__stog))
        elem = self.__stog.pop()
        eprint("Element je: " + str(elem))
        self.__stog.append(elem)
        return elem

    def ispis(self):
        self.__ispis += str(self.__trenutno_stanje) + "#"
        for znak in self.__stog[0]:
            self.__ispis += str(znak)
        self.__ispis += "|"
        return
