#!/bin/bash

mkdir -p results
rm results/output.txt results/expected.txt results/logs.txt results/inputsets.txt

for f in lab4_primjeri/**
do	
	if [[ "$f" != *":"* ]]
	then
		dir=./$f/test

		echo "running $dir.in"
		echo "running $dir.in" >> results/output.txt
		echo "running $dir.in" >> results/logs.txt
		echo "running $dir.in" >> results/inputsets.txt
		cat $dir.in >> results/inputsets.txt

		python3 Parser.py < $dir.in >> results/output.txt 2>> results/logs.txt
		
		echo -e "\n" >> results/output.txt
		echo -e "\n" >> results/logs.txt
		echo -e "\n" >> results/inputsets.txt

		echo "running $dir.out"
		cat $dir.out >> results/expected.txt
		echo -e "\n" >> results/expected.txt
	fi
done	

#find . -maxdepth 1 -type f -exec python3 ~/Documents/Development/FER/UTR/Lab1/SimEnka.py < '{}' >> ~/Documents/Development/FER/UTR/Lab1/out.txt \;
