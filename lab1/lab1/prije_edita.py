from __future__ import print_function
import sys
from eNFA import eNFA

EPSILON = "$"
EMPTY_SET = "#"

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def zapocni():
    ulazni_nizovi = obradi(input(), "|")
    stanja = obradi(input(), ",")
    abeceda = obradi(input(), ",")
    prihvatljiva = obradi(input(), ",")
    pocetno = input()

    prijelazi = []
    for line in sys.stdin:
        prijelazi.append(line)
    abeceda.append(EPSILON)

    amen = eNFA(ulazni_nizovi, stanja, abeceda, prijelazi, prihvatljiva, pocetno)
    return amen

def obradi(string, delimiter):
    lista = string.split(delimiter)
    return lista

def iteriraj_po_epsilon(nfa, trenutna_stanja):
    nova_stanja = set()
    
    for stanje in trenutna_stanja:
        kljuc = stanje + "," + EPSILON

        niz_stanja_nakon_epsilona = nfa.prijelazi.get(kljuc)
        
        if niz_stanja_nakon_epsilona != None:
            for novo in niz_stanja_nakon_epsilona:
                nova_stanja.add(novo)
    
    return nova_stanja

def mijenjaj_stanja(nfa, trenutna_stanja, znak):
    nova_stanja = set()

    for stanje in trenutna_stanja:
        kljuc = stanje + "," + znak

        niz_stanja_nakon_epsilona = nfa.prijelazi.get(kljuc)

        if niz_stanja_nakon_epsilona != None:
            for novo in niz_stanja_nakon_epsilona:
                if novo != EMPTY_SET:
                    nova_stanja.add(novo)

    return nova_stanja

def ispis_stanja(trenutna_stanja):
    trenutna_stanja = sorted(trenutna_stanja)
    konacni = ""
    for stanje in trenutna_stanja:
        konacni += stanje + ","
    
    konacni = konacni[:-1] + "|"

    if konacni == "|":
        konacni = "#|"
    
    return konacni
