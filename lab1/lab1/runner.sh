#!/bin/bash

cd ~/Documents/Development/FER/UTR/Lab1/

mkdir -p results
rm results/output.txt results/expected.txt results/logs.txt results/inputsets.txt

for f in testni_primjeri/**
do	
	if [[ "$f" != *":"* ]]
	then
		dir=./$f/test

		echo "running $dir.a"
		echo "running $dir.a" >> results/output.txt
		echo "running $dir.a" >> results/logs.txt
		echo "running $dir.a" >> results/inputsets.txt
		cat $dir.a >> results/inputsets.txt

		python3 SimEnka.py < $dir.a >> results/output.txt 2>> results/logs.txt
		
		echo -e "\n" >> results/output.txt
		echo -e "\n" >> results/logs.txt
		echo -e "\n" >> results/inputsets.txt

		echo "running $dir.b"
		cat $dir.b >> results/expected.txt
		echo -e "\n" >> results/expected.txt
	fi
done	

#find . -maxdepth 1 -type f -exec python3 ~/Documents/Development/FER/UTR/Lab1/SimEnka.py < '{}' >> ~/Documents/Development/FER/UTR/Lab1/out.txt \;
