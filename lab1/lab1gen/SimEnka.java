import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class SimEnka {

	public static void main(String[] args) {
		Scanner skener = new Scanner(System.in);
		String linija = skener.nextLine();
		String[] ulazniNizovi = linija.split("\\|");
		linija = skener.nextLine();
		Set<String> skupStanja = new TreeSet<String>();
		for (String s : linija.split("\\,")) {
			skupStanja.add(s);
		}
		skupStanja.add("#");
		linija = skener.nextLine();
		Set<String> simboliAbecede = new TreeSet<String>();
		for (String s : linija.split("\\,")) {
			simboliAbecede.add(s);
		}
		linija = skener.nextLine();
		Set<String> prihvatljivaStanja = new TreeSet<String>();

		for (String s : linija.split("\\,")) {
			prihvatljivaStanja.add(s);
		}
		linija = skener.nextLine();
		String pocetnoStanje = linija;

		Map<String, Map<String, String[]>> mapaPrijelaza = new TreeMap<>();
		while (skener.hasNextLine() && !(linija = skener.nextLine()).isEmpty()) {

			String pocetno = linija.split(",", 2)[0];
			String prijelaz = linija.split(",", 2)[1];
			String znak = prijelaz.split("->")[0];
			String novoSt = prijelaz.split("->")[1];
			String[] novaSt = novoSt.split(",");
			Map<String, String[]> mapaZnakStanje = new TreeMap<>();
			if (mapaPrijelaza.containsKey(pocetno)) {
				mapaZnakStanje = mapaPrijelaza.get(pocetno);
			}
			mapaZnakStanje.put(znak, novaSt);
			mapaPrijelaza.put(pocetno, mapaZnakStanje);
		}

		for (int i = 0; i < ulazniNizovi.length; i++) {
			Set<String> trenutnaStanja = new TreeSet<String>();

			trenutnaStanja.add(pocetnoStanje);
			Set<String> sljedecaStanja = new TreeSet<String>();
			List<String> listaZnakova = new ArrayList<>();
			for (String s : ulazniNizovi[i].split(",")) {
				listaZnakova.add(s);
			}
			for (String znak : listaZnakova) {
				int brojPromjena = 1;
				while (brojPromjena != 0) {
					brojPromjena = 0;
					List<String> epsilonStanja = new ArrayList<String>();
					for (String s : trenutnaStanja) {
						if (mapaPrijelaza.containsKey(s)) {
							if (mapaPrijelaza.get(s).containsKey("$")) {
								for (String state : mapaPrijelaza.get(s).get(
										"$")) {
									if (!trenutnaStanja.contains(state)) {
										epsilonStanja.add(state);

										brojPromjena++;
									}
								}
							}
						}
					}
					for (String state : epsilonStanja) {
						trenutnaStanja.add(state);
					}

				}

				for (String stanje : trenutnaStanja) {
					if (mapaPrijelaza.containsKey(stanje)) {
						if (mapaPrijelaza.get(stanje).containsKey(znak)) {
							String[] tmp = mapaPrijelaza.get(stanje).get(znak);
							for (int z = 0; z < tmp.length; z++) {
								sljedecaStanja.add(tmp[z]);
							}
						}
					}
				}
				if (sljedecaStanja.isEmpty()) {
					sljedecaStanja.add("#");
				}
				int y = 0;
				for (String s : trenutnaStanja) {
					y++;
					if (y < trenutnaStanja.size()) {
						System.out.print(s + ",");
					} else {
						System.out.print(s);
					}
				}
				System.out.print("|");

				trenutnaStanja.removeAll(trenutnaStanja);
				trenutnaStanja.addAll(sljedecaStanja);
				sljedecaStanja.removeAll(sljedecaStanja);
			}
			int brojPromjena = 1;
			while (brojPromjena != 0) {
				brojPromjena = 0;
				List<String> epsilonStanja = new ArrayList<String>();
				for (String s : trenutnaStanja) {
					if (mapaPrijelaza.containsKey(s)) {
						if (mapaPrijelaza.get(s).containsKey("$")) {
							for (String state : mapaPrijelaza.get(s).get("$")) {
								if (!trenutnaStanja.contains(state)) {

									epsilonStanja.add(state);
									brojPromjena++;
								}
							}
						}
					}
				}
				for (String state : epsilonStanja) {
					trenutnaStanja.add(state);
				}

			}
			int y = 0;
			for (String s : trenutnaStanja) {
				y++;
				if (y < trenutnaStanja.size()) {
					System.out.print(s + ",");
				} else {
					System.out.print(s);
				}

			}
			System.out.println();

		}
		skener.close();
	}
}
