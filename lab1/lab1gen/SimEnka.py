from metode import *

nfa = zapocni()

for niz in nfa.ulazni_nizovi:

    konacni_niz = ""
    trenutna_stanja = set()
    trenutna_stanja.add(nfa.pocetno_stanje)

    for znak in niz:
        e_stanja = iteriraj_po_epsilon(nfa, trenutna_stanja)
        for stanje in e_stanja:
            trenutna_stanja.add(stanje)

        konacni_niz += ispis_stanja(trenutna_stanja)

        nova_stanja = mijenjaj_stanja(nfa, trenutna_stanja, znak)
        trenutna_stanja = nova_stanja

    #e-prijelaz nakon konacnog prijelaza
    e_stanja = iteriraj_po_epsilon(nfa, trenutna_stanja)
    for stanje in e_stanja:
        trenutna_stanja.add(stanje)

    konacni_niz += ispis_stanja(trenutna_stanja)
    print(konacni_niz[:-1])
